import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TabsComponent } from "./tabs/tabs.component";
import { TabComponent } from "./tabs/tab/tab.component";
import { SelectComponent } from "./select/select.component";
import { CheckboxComponent } from "./checkbox/checkbox.component";
import { TableComponent } from "./table/table.component";

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TabsComponent,
    TabComponent,
    SelectComponent,
    CheckboxComponent,
    TableComponent,
  ],
  declarations: [
    TabsComponent,
    TabComponent,
    SelectComponent,
    CheckboxComponent,
    TableComponent,
  ],
})
export class SharedModule {}
