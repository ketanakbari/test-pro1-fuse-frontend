import { Component, Input, OnInit } from "@angular/core";
import { SelectOption } from "./models/select-option.model";

@Component({
  selector: "app-select",
  templateUrl: "./select.component.html",
  styleUrls: ["./select.component.scss"],
})
export class SelectComponent implements OnInit {
  @Input() label: string = "";
  @Input() options: SelectOption[] = [];
  
  constructor() {}

  ngOnInit(): void {}
}
