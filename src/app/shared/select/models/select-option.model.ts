export interface SelectOption {
  value: number | string;
  text: string;
}
