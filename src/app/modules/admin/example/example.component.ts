import { Component, ViewEncapsulation } from "@angular/core";

@Component({
  selector: "example",
  templateUrl: "./example.component.html",
  styleUrls: ["./example.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class ExampleComponent {
  childs = [
    {
      value: 1,
      text: "One",
    },
    {
      value: 2,
      text: "Two",
    },
    {
      value: 3,
      text: "Three",
    },
  ];
  /**
   * Constructor
   */
  constructor() {}
}
